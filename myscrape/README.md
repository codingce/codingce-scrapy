# 爬虫项目
# 新建爬虫项目
```bash
scrapy startproject xxx 
```

# 明确目标
编写items.py 明确你想爬取的目标

# 制作爬虫
spiders/xxxspider.py 制作爬虫开始爬取的网址

# 处理内容
pipelines.py 设计管道存储爬取内容

# 创建crawl爬虫模板
```bash
scrapy genspider -t crawl wangyi 163.com
```


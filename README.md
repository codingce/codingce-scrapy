Python Django面向新闻文本的人物身份信息抽取系统的设计与实现
====================

目标
----
- [X] 新闻采集
- [X] 使用Django展示新闻
- [X] 后台管理新闻
- [X] 管理新闻模板

项目结构
-------
myscrape/ 是一个scrapy项目,爬取网易新闻并保存到数据库
mypost/ 是一个django项目,展示和管理爬取到的新闻

注意事项
-------
scrapy在Windows下如果安装失败,最简单的解决方法是手动安装编译过的Twisted

Django运行
-------
    $ pip install -r requirements.txt
    
    $ cd mypots 
    $ python3 manage.py runserver localhost:8081

管理员用户 admin 密码 admin

Scrape爬取运行
---
    $ cd myscrape
    $ scrapy crawl myscrape
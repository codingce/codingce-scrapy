from django.urls import path

from . import views
from django.contrib import admin

urlpatterns = [
    # path('list/', views.listorders),
    path('', views.main, name="main"),
    # path('', views.mypage(1), name="mypage1"),
    # path("page/<pindex>", views.mypage, name="mypage")
    path(r'', views.home, name='home'),
    path(r'mypost/', views.mypost, name='mypost'),

    # path('', views.PostList.as_view(), name='index'),
    path('admin/', admin.site.urls),
    path('posts/', views.PostList.as_view(), name='posts'),

    path('myc', views.myc, name='myc'),

    path('posts/<int:pk>', views.post, name='post'),
    path('recover', views.recover, name='recover'),


    # 分页
    path("page/<pindex>", views.mypage, name="mypage"),
    path("page/<pindex>/<key>", views.pagesearch, name="pagesearch"),
    path("search", views.search, name="search"),


    # 测试
    path("test", views.test, name="test"),
    path("echarts", views.echarts, name="echarts"),
]

from django.contrib import admin

# Register your models here.

# 然后我们需要修改应用里面的 管理员 配置文件 common/admin.py，
# 注册我们定义的model类。这样Django才会知道

from django.contrib import admin

from .models import Post
from .models import Template

admin.site.register(Post)
admin.site.register(Template)

# DjangWeb展示
# 运行项目
```bash
    python3 manage.py runserver localhost:8081
```
    
# 配置
ALLOWED_HOSTS = ['192.168.1.100', 'localhost']

# 创建项目app(application)
## 模块化项目
```bash
    python3 manage.py startapp sales
```

# 创建超级管理员账号(本系统)
账号/密码：admin/admin
```bash
    python manage.py createsuperuser
```
     
# 创建数据库并生成相应的表
```bash
    python3 manage.py migrate
```


# 数据库修改
```bash
    python3 manage.py makemigrations
    
    python3 manage.py migrate
```